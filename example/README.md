# Example plugin

This folder provides a fully working example plugin that you can build and run with docker or podman. It assumes that you already have the dev environment of repository running to attach to that RabbitMQ server. But you can also setup and run your own RabbitMQ. You just have to configure the HCA then according to configuration parameters describe in the main [README](../README.md#configuring-the-hca).

To run this example just start it with docker compose:

```
docker-compose up

2024-02-29 14:26:35,146	INFO	main.py	run	Starting receiver...
2024-02-29 14:26:35,155	INFO	main.py	run	Start receiving messages...

```

You can then try to send and receive messages according to [README](../README.md#sending-test-messages).