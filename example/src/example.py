import logging
import uuid
from random import randint
from time import sleep

from pydantic import BaseModel, Field, TypeAdapter, ValidationError

from helmholtz_cloud_agent.core.main import HCAApplication
from helmholtz_cloud_agent.messages import (
    ResourceAllocateV1,
    ResourceCreatedV1,
)

# Global logger
logging.basicConfig(
    format="%(asctime)s\t%(levelname)s\t%(filename)s\t%(funcName)s\t%(message)s"
)

logger = logging.getLogger("hca")


class GroupStorageAlreadyExistsError(Exception):
    def __init__(
        self: "GroupStorageAlreadyExistsError",
        folder_name: str,
        message: str = "Group storage already exists",
    ) -> None:
        self.folder_name = folder_name
        self.message = message
        super().__init__(self.message)

    def __str__(self: "GroupStorageAlreadyExistsError") -> str:
        return f"{self.message}: {self.folder_name}"


class ResourcesNotAvailableError(Exception):
    def __init__(
        self: "ResourcesNotAvailableError",
        ram: int,
        cpu: int,
        storage: int,
        message: str = "The requested resources are not available",
    ) -> None:
        self.ram = ram
        self.cpu = cpu
        self.storage = storage
        self.message = message
        super().__init__(self.message)

    def __str__(self: "ResourcesNotAvailableError") -> str:
        return f"{self.message}: {self.cpu}, {self.ram}, {self.storage}"


class UnknownResourceSpecTypeError(Exception):
    def __init__(
        self: "UnknownResourceSpecTypeError",
        spec_type: str,
        message: str = "Unknown resource spec type",
    ) -> None:
        self.spec_type = spec_type
        self.message = message
        super().__init__(self.message)

    def __str__(self: "UnknownResourceSpecTypeError") -> str:
        return f"{self.message}: {self.spec_type}"


class ResourceSpecTypeEmptyError(Exception):
    def __init__(
        self: "ResourceSpecTypeEmptyError",
        message: str = "Resource spec type is empty. Request cannot be handled.",
    ) -> None:
        self.message = message
        super().__init__(self.message)

    def __str__(self: "ResourceSpecTypeEmptyError") -> str:
        return f"{self.message}"


class QuotaV1(BaseModel):
    unit: str = Field(..., title="Unit of the resource")
    value: int = Field(..., title="Value of the resource")


class GroupStorageResourceSpecV1(BaseModel):
    desired_name: str = Field(..., title="Desired name of the folder")
    quota: QuotaV1 = Field(..., title="Requested Quota")


class ComputeResourceSpecV1(BaseModel):
    cpu: QuotaV1 = Field(..., title="Requested CPU")
    ram: QuotaV1 = Field(..., title="Requested RAM")
    storage: QuotaV1 = Field(..., title="Requested Storage")


app = HCAApplication()


@app.handle(message_type="ResourceAllocateV1")
def resource_allocate_v1(
    correlation_id: str, payload: ResourceAllocateV1
) -> ResourceCreatedV1:
    """Example message handler for a ResourceAllocateV1 message"""
    if not payload.type:
        raise ResourceSpecTypeEmptyError

    logger.info(
        "Provision resource of type '%s'. Message correlation_id: '%s'",
        payload.type,
        correlation_id,
    )
    random_threshold = 3
    dataclass_validator: (
        TypeAdapter[GroupStorageResourceSpecV1] | TypeAdapter[ComputeResourceSpecV1]
    )
    spec: GroupStorageResourceSpecV1 | ComputeResourceSpecV1
    try:
        if payload.type == "GroupStorageResourceSpecV1":
            dataclass_validator = TypeAdapter(GroupStorageResourceSpecV1)
            spec = dataclass_validator.validate_python(payload.specification)
            logger.info(" [Resource specification]")
            logger.info(" -> Type: %s", payload.type)
            logger.info(" -> Desired name: %s", spec.desired_name)
            logger.info(" -> Quota: %s%s", spec.quota.value, spec.quota.unit)
            if randint(0, 10) < random_threshold:  # nosec
                raise GroupStorageAlreadyExistsError(spec.desired_name)
        elif payload.type == "ComputeResourceSpecV1":
            dataclass_validator = TypeAdapter(ComputeResourceSpecV1)
            spec = dataclass_validator.validate_python(payload.specification)
            logger.info(" [Resource specification]")
            logger.info(" -> Type: %s", payload.type)
            logger.info(" -> RAM: %s%s", spec.ram.value, spec.ram.unit)
            logger.info(" -> CPU: %s%s", spec.cpu.value, spec.cpu.unit)
            logger.info(
                " -> Storage: %s%s",
                spec.storage.value,
                spec.storage.unit,
            )
            if randint(0, 10) < random_threshold:  # nosec
                raise ResourcesNotAvailableError(
                    spec.ram.value,
                    spec.cpu.value,
                    spec.storage.value,
                )
        else:
            logger.error("Unknown resource spec '%s'", payload.type)
            raise UnknownResourceSpecTypeError(payload.type)
    except ValidationError:
        logger.exception("cannot validate request specification")
    sleep(10)
    return ResourceCreatedV1(id=str(uuid.uuid4()))


app.run()
