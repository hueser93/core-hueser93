# Helmholtz Cloud Agent
This is the documentation for the **H**elmholtz **C**loud **A**gent. Everything you need to operate HCA and to get started to write your own HCA plugins.

## Getting started

You can install the python package from this PyPI repository:

```bash
pip install helmholtz-cloud-agent --index-url https://codebase.helmholtz.cloud/api/v4/projects/2482/packages/pypi/simple
```

Then you can just register handlers for messages coming from the Cloud Portal with a simple decorator like this:

```python
from helmholtz_cloud_agent.core.main import HCAApplication


app = HCAApplication()


@app.handle(message_type="ResourceAllocateV1")
def resource_allocate_v1(
    correlation_id: str, payload: ResourceAllocateV1
) -> ResourceCreatedV1:
    # your handling code here


app.run()
```

That's it. Whenever a message of type `ResourceAllocateV1` is received your handler will be called and you can write your own to provision your resources.

## Configuring the HCA

| Variable  | Default | Description  |
|---|---|---|
| HCA_USERNAME  | "hca" | The username to authenticate to RabbitMQ. Will be assigned for each plugin.  |
| HCA_PASSWORD  | "supersecret"  | The password to authenticate to RabbitMQ. Will be assigned for each plugin. |
| HCA_SERVICE_ID  | "d4c32c0e-6375-4f31-9360-5d5d62172703" | The ID in Cloud Portal for this service. It is used to match messages to the right plugin.  |
| HCA_RABBITMQ_USE_SSL  | False | Use SSL to connect to RabbitMQ. |
| HCA_RABBITMQ_CA_FILE  | "/etc/ssl/certs/ca-certificates.crt"  | Location of the CA bundle to use to verify the server certificate. |
| HCA_RABBITMQ_HOSTNAME  | "hifis-rabbitmq.desy.de" | The hostname of the RabbitMQ server. |
| HCA_RABBITMQ_PORT  | 5672  | The port of the RabbitMQ server. |
| HCA_RABBITMQ_VIRTUAL_HOST  | "test-provider"  | The RabbitMQ virtual host. Each provider will get its own. |
| HCA_RABBITMQ_RETRY_DELAY  | 20 | Time to wait to retry connection attempt to RabbitMQ in case of failure. |
| HCA_RABBITMQ_CONNECTION_ATTEMPTS  | 3 | Number of connections attempts to RabbitMQ |
| HCA_RABBITMQ_HEARTBEAT  | 0 | Enable RabbitMQ heartbeat mechanism. https://pika.readthedocs.io/en/stable/modules/parameters.html?highlight=heartbeat#pika.connection.ConnectionParameters.heartbeat |
| HCA_LOG_LEVEL  | "INFO" | The output log level. |


## Running the HCA

You can easily deploy and run your HCA using docker containers. An example how to use write a package a plugin can be found in the [example](example/) folder in this repository.


## High level architecture
```
┌──────────────────────────────────────────────────┐
│                                                  │
│  ┌────────────────┐          ┌────────────────┐  │
│  │                │          │                │  │
│  │                │ ◄─────── │   Helmholtz    │  │
│  │    RabbitMQ    │          │                │  │
│  │                │ ───────► │  Cloud Portal  │  │
│  │                │          │                │  │
│  └────────────────┘          └────────────────┘  │
│           ▲                                      │
│       │   │   cloud.helmholtz.de                 │
└───────┼───┼──────────────────────────────────────┘
        │   │
        │   │
────────┼───┼──Internet/Helmholtz-VPN ──────────────
        │   │
        │   │
┌───────┼───┼──────────────────────────────────────┐
│       │   │                                      │
│       ▼                                          │
│  ┌──────────────┐           ┌─────────────────┐  │
│  │             ◄├───────────│                 │  │
│  │  HCA A       │           │    Service A    │  │
│  │            ──┼──────────►│                 │  │
│  └──────────────┘           ├─────────────────┤  │
│  ┌──────────────┐           ┌─────────────────┐  │
│  │             ◄├───────────│                 │  │
│  │  HCA B       │           │    Service B    │  │
│  │            ──┼──────────►│                 │  │
│  └──────────────┘           ├─────────────────┤  │
│  ┌──────────────┐           ┌─────────────────┐  │
│  │             ◄├───────────│                 │  │
│  │  HCA C       │           │    Service C    │  │
│  │            ──┼──────────►│                 │  │
│  └──────────────┘           └─────────────────┘  │
│                                                  │
│                Service Provider                  │
└──────────────────────────────────────────────────┘
```

## Developing your own plugins

### Plugin mechanism
The HCA and it's plugins are written in Python (>= 3.12). Each plugin has a unique name and subscribes to events and one or more services. The name of your plugin should be in PascalCase.



#### Sending test messages
Use the RabbitMQ management web ui which is deployed with the development environment.
You can open the management interface by going to http://localhost:15672. Use `RABBITMQ_DEFAULT_USER` and `RABBITMQ_DEFAULT_PASS` from the [docker-compose.yml](dev/docker-compose.yml) to log in.
![Rabbitmq Login](docs/images/rabbitmq_login.png)

You can find an overview of all queues under "Queues and Streams". If you already started the HCA you will find one queue: `to_hca`. After you sent a message and the HCA sent something back another queue `to_portal` will show up in the table. To send test message to the HCA click on the name of the `to_hca` queue.

![Rabbitmq Queues](docs/images/rabbitmq_queues.png)

You can then send message using the "Publish message" interface. A message needs two properties:
- `service_id`: Tells the HCA which plugin should handle the request.
- `type`: Tells the plugin with kind of message it has to handle.

The payload depends on the type of the message. The example plugin can handle `ResourceAllocateV1` requests like shown in the picture below. The example payload can also be found in [schema definition](schemas/ResourceAllocateV1.schema.json).

![Rabbitmq Send Messages](docs/images/rabbitmq_send_message.png)

After the message has been published the example plugin will send a randomized answer to the `to_portal` queue. The answer can be received directly from the queue using the "Get messages" interface.

![Rabbitmq Receive Message](docs/images/rabbitmq_receive_message.png)
