# Helmholtz Cloud Agent (HCA) protocol specification

## Disclaimer
This specification is currently just a draft. Things may change heavily and we have currently no clue of the concrete message types used by this protocol. The provided messages are just examples.

## About the HCA
The *HCA* is the reference implementation of this specification and it's a piece of software written in Python 3.9 which runs at a service provider to receive messages from the *Helmholtz Cloud Portal* and execute arbitary tasks. Read more about the HCA [here](../README.md).

## Purpose of this document
This document should enable the reader to implement a client similar to the HCA as it describes the way the HCA communicates with the *Helmholtz Cloud Portal*.

## High level architecture
The *HCA* and the *Helmholtz Cloud Portal* communicate over a central RabbitMQ hosted by the *Helmholtz Cloud*.
```
┌──────────────────────────────────────────────────┐
│                                                  │
│  ┌────────────────┐          ┌────────────────┐  │
│  │                │          │                │  │
│  │                │ ◄─────── │   Helmholtz    │  │
│  │    RabbitMQ    │          │                │  │
│  │                │ ───────► │  Cloud Portal  │  │
│  │                │          │                │  │
│  └────────────────┘          └────────────────┘  │
│           ▲                                      │
│       │   │   cloud.helmholtz.de                 │
└───────┼───┼──────────────────────────────────────┘
        │   │
        │   │
────────┼───┼──Internet/Helmholtz-VPN ──────────────
        │   │
        │   │
┌───────┼───┼──────────────────────────────────────┐
│       │   │                                      │
│       ▼                                          │
│  ┌──────────────┐           ┌─────────────────┐  │
│  │              │           │    Service A    │  │
│  │              │           ├─────────────────┤  │
│  │              │ ─────────►├─────────────────┤  │
│  │      HCA     │           │    Service B    │  │
│  │              │ ◄──────── ├─────────────────┤  │
│  │              │           ├─────────────────┤  │
│  │              │           │    Service C    │  │
│  └──────────────┘           └─────────────────┘  │
│                                                  │
│                Service Provider                  │
└──────────────────────────────────────────────────┘
```

## Message broker (RabbitMQ)
### Connection details
Each service provider receives a dedicated user and dedicated vhost to communicate with the central RabbitMQ instance. The username and vhost you use to connect is the equal to the **serviceProvideruUuid** you receive when registering as a service provider. You will also receive a secret which will be used as the password to your RabbitMQ user.  

### Queues
The *Helmholtz Portal* declares two queues for each service provider in the service providers dedicated vhost:
- **to_hca**  
This queue is the backlog queue for the HCA. The portal pushes messages onto this queue.
- **to_portal**  
This is the queue where the HCA pushes messages to the portal.

### Message properties
The portal only read and writes the following two *RabbitMQ* message properties:
- **Type**  
The name of the message type. See the [documentation of messages](schemas/README.md) for available messages and their format.
- **App ID**  
The name of the service  the message was meant for e.g. "nextcloud". Each service gets a unique name in the portal. See [registered service list](https://cloud.helmholtz.de#insert-services-link) for all available services.
  
### Acknowledging messages
Coming soon, we are still figuring out a proper acknowledgment policy/mechanism.