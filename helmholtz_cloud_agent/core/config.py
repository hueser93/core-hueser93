from pydantic_settings import BaseSettings, SettingsConfigDict


class Config(BaseSettings):
    username: str = "hca"
    password: str = "supersecret"
    service_id: str = "d4c32c0e-6375-4f31-9360-5d5d62172703"
    dev_mode: bool = False
    rabbitmq_use_ssl: bool = False
    rabbitmq_cafile: str = "/etc/ssl/certs/ca-certificates.crt"
    rabbitmq_hostname: str = "hifis-rabbitmq.desy.de"
    rabbitmq_port: int = 5672
    rabbitmq_virtual_host: str = "test-provider"
    rabbitmq_retry_delay: int = 20
    rabbitmq_connection_attempts: int = 3
    rabbitmq_heartbeat: int = 0
    log_level: str = "INFO"
    model_config = SettingsConfigDict(env_prefix="hca_")
