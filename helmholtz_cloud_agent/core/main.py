import logging
import ssl
from time import sleep
from typing import Callable

import pika
import pika.exceptions
from pydantic import BaseModel, TypeAdapter, ValidationError

from helmholtz_cloud_agent.core.config import Config
from helmholtz_cloud_agent.messages import messages

# Name of client and server queue
receiver_queue, sender_queue = "to_hca", "to_portal"

# Global logger
logging.basicConfig(
    format="%(asctime)s\t%(levelname)s\t%(filename)s\t%(funcName)s\t%(message)s"
)

logger = logging.getLogger("hca")


class HCAApplication:
    def __init__(self: "HCAApplication", *, run_once: bool = False) -> None:
        self.conf = Config()
        self._run_once = run_once
        if self.conf.dev_mode:
            for key, value in self.conf.__dict__.items():
                logger.debug("%s: %s", key, value)
        logger.setLevel(self.conf.log_level)

        # RabbitMQ connection parameters
        credentials = pika.PlainCredentials(self.conf.username, self.conf.password)
        if self.conf.rabbitmq_use_ssl:
            context = ssl.create_default_context(cafile=self.conf.rabbitmq_cafile)
            ssl_options = pika.SSLOptions(context, self.conf.rabbitmq_hostname)
            self.pika_params = pika.ConnectionParameters(
                host=self.conf.rabbitmq_hostname,
                port=self.conf.rabbitmq_port,
                credentials=credentials,
                virtual_host=self.conf.rabbitmq_virtual_host,
                ssl_options=ssl_options,
                retry_delay=self.conf.rabbitmq_retry_delay,
                connection_attempts=self.conf.rabbitmq_connection_attempts,
                heartbeat=self.conf.rabbitmq_heartbeat,
            )
        else:
            self.pika_params = pika.ConnectionParameters(
                host=self.conf.rabbitmq_hostname,
                port=self.conf.rabbitmq_port,
                credentials=credentials,
                virtual_host=self.conf.rabbitmq_virtual_host,
                retry_delay=self.conf.rabbitmq_retry_delay,
                connection_attempts=self.conf.rabbitmq_connection_attempts,
                heartbeat=self.conf.rabbitmq_heartbeat,
            )
        self.handlers: dict[str, Callable[..., BaseModel]] = {}

    def handle(
        self: "HCAApplication", **kwargs: str
    ) -> Callable[[Callable[..., BaseModel]], None]:
        """register a callback function that will be called when a message of the specified type has been received"""

        def inner(func: Callable[..., BaseModel]) -> None:
            message_type = kwargs["message_type"]
            self.handlers[message_type] = func

        return inner

    def send_message(
        self: "HCAApplication", correlation_id: str | None, message: BaseModel
    ) -> None:
        """send a message back to the portal"""
        # Check if passed message is part of the HCA library
        try:
            getattr(messages, message.__class__.__name__)
        except AttributeError:
            msg = "Cannot send message. Message type not part of HCA messages library."
            logger.exception(msg=msg)

        # In case the connection to rabbitmq is down this loop tries to create
        # a new connection every 30 seconds until it can finally send the message
        while True:
            try:
                logger.debug("try sending message...")
                # Initialize connection to RabbitMQ
                connection = pika.BlockingConnection(parameters=self.pika_params)
                channel = connection.channel()

                resp_props = pika.BasicProperties(
                    type=message.__class__.__name__,
                    message_id=correlation_id,
                    delivery_mode=pika.DeliveryMode.Persistent,  # makes the broker persist the message to disk so it survives broker restarts
                )
                channel.basic_publish(
                    exchange="",
                    routing_key=sender_queue,
                    body=str.encode(message.model_dump_json()),
                    properties=resp_props,
                )
                break
            except pika.exceptions.AMQPConnectionError:
                logger.exception("Connection error. Retry connection.")
                sleep(30)
            finally:
                logger.debug("message sent successfully...")
                channel.close()
                connection.close()

    def on_message(
        self: "HCAApplication",
        channel: pika.adapters.blocking_connection.BlockingChannel,
        method: pika.spec.Basic.Deliver,
        props: pika.BasicProperties,
        body: bytes,
    ) -> None:
        """handles incoming messages from portal. Checks if it has a valid message type and then calls a registered handler callback"""

        # crash if message has no type set (this should not happen)
        if props.type is None:
            channel.basic_ack(method.delivery_tag)
            msg = "No type set in message properties"
            raise RuntimeError(msg)

        # Handle Ping messages
        if props.type == "PingV1":
            self.send_message(
                correlation_id=None,
                message=messages.PingV1(message="still alive"),
            )
            channel.basic_ack(method.delivery_tag)
            return

        if "service_id" not in props.headers:
            msg = "cannot find service_id in headers. Discard message"
            logger.error(msg)
            channel.basic_ack(method.delivery_tag)
            return

        logger.debug(
            "Received new message for service '%s' (message type '%s')",
            props.headers["service_id"],
            props.type,
        )

        # Check if received message is meant for this HCA instance
        if props.headers["service_id"] != self.conf.service_id:
            logger.warning(
                "Received message for for wrong service id expected %s, received %s",
                self.conf.service_id,
                props.headers["service_id"],
            )
            return

        # Check if a handler is available for given message type
        if props.type not in self.handlers:
            logger.warning(
                "Warning, no handler for service '%s' (message type '%s')",
                props.headers["service_id"],
                props.type,
            )
            return

        # Check if given message type is available in the hca message library
        try:
            dataclass_type = getattr(messages, props.type)
        except AttributeError as ex:
            logger.exception("Unknown type in message properties")
            message = messages.ErrorV1(
                type=type(ex).__name__,
                message=f"type '{props.type}' in message properties unknown",
            )
            self.send_message(
                correlation_id=props.correlation_id,
                message=message,
            )
            return

        dataclass_validator = TypeAdapter(dataclass_type)
        try:
            body = dataclass_validator.validate_json(body)
        except ValidationError:
            msg = "Cannot validate given message body"
            logger.exception(msg=msg)
            channel.basic_ack(method.delivery_tag)
            raise

        try:
            result = self.handlers[props.type](props.correlation_id, body)
        except Exception as ex:
            logger.exception("Error handling message")
            result = messages.ErrorV1(type=type(ex).__name__, message=str(ex))

        # manually ack message after handler has run
        channel.basic_ack(method.delivery_tag)
        self.send_message(
            correlation_id=props.correlation_id,
            message=result,
        )

    def run(
        self: "HCAApplication",
    ) -> None:
        """Starts the receiver loops to get messages from portal"""
        logger.info("Starting receiver...")
        # Initialize connection to RabbitMQ
        try:
            while True:
                connection = pika.BlockingConnection(parameters=self.pika_params)
                channel = connection.channel()

                # if _run_once set only handle one message and then return
                if self._run_once:
                    method, props, body = channel.basic_get(receiver_queue)
                    # if there is no message basic_get will still return with None, so need to check here
                    if method:
                        self.on_message(
                            channel=channel, method=method, props=props, body=body
                        )
                        break
                    sleep(1)
                # otherwise continously consume messages from RabbitMQ
                else:
                    channel.basic_consume(
                        queue=receiver_queue,
                        on_message_callback=lambda c, m, p, b: self.on_message(
                            c, m, p, b
                        ),
                    )
                    logger.info("Start receiving messages...")
                    channel.start_consuming()
        except pika.exceptions.ConnectionClosedByBroker:
            logger.warning("Connection was closed by broker. Retry connection.")
        except pika.exceptions.AMQPConnectionError:
            logger.exception("Connection error. Retry connection.")
        finally:
            channel.close()
            connection.close()
